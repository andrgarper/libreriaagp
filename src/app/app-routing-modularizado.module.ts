import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {ListadoComponent} from "./libros/listado/listado.component";
import {InicioComponent} from "./inicio/inicio.component";
import {DetallesComponent} from "./libros/detalles/detalles.component";
import {ListadoClientesComponent} from "./clientes/listado-clientes/listado-clientes.component";
import {FormClientesComponent} from "./clientes/form-clientes/form-clientes.component";
import {DetallesClientesComponent} from "./clientes/detalles-clientes/detalles-clientes.component";
import {ListadoPrestamoComponent} from "./prestamo/listado-prestamo/listado-prestamo.component";
import {FormPrestamoComponent} from "./prestamo/form-prestamo/form-prestamo.component";
import {FormlibroComponent} from "./libros/formlibro/formlibro.component";
import {MainLoginComponent} from "./login/main-login/main-login.component";
import {RegisterComponent} from "./login/register/register.component";
import {GuardGuard} from "./guards/guard.guard";


const routes: Routes = [
  {
    path: 'clientes',
    loadChildren: () => import('./clientes/clientes.module').then((m) => m.ClientesModule),
    component: ListadoClientesComponent,
    canActivate:[GuardGuard]
  },
  {
    path: 'libros',
    loadChildren: () => import('./libros/libros.module').then((m) => m.LibrosModule),
    component: ListadoComponent,
    canActivate:[GuardGuard]
  },

  //rutas de clientes

  {
    path:"clientes",
    component: ListadoClientesComponent,
    canActivate:[GuardGuard]
  },{
    path:"clientes/nuevo",
    component: FormClientesComponent,
    canActivate:[GuardGuard]
  },{
    path:"clientes/detalles",
    component: DetallesClientesComponent,
    canActivate:[GuardGuard]
  },{
    path:"clientes/actualizar",
    component: FormClientesComponent,
    canActivate:[GuardGuard]
  }

  //rutas de libro
   ,{
    path:"libros",
    component: ListadoComponent,
    canActivate:[GuardGuard]
  },{
    path:"libros/nuevo",
    component: FormlibroComponent,
    canActivate:[GuardGuard]
  },{
    path:"libros/detalles",
    component: DetallesComponent,
    canActivate:[GuardGuard]
  },{
    path:"libros/actualizar",
    component: FormlibroComponent,
    canActivate:[GuardGuard]
  },
  //rutas inmodularizadas
  {
    path:"login",
    component:MainLoginComponent
  },
  {
    path:"register",
    component:RegisterComponent
  },
  {
    path:"inicio",
    component: InicioComponent,
    canActivate:[GuardGuard]
  },{
    path:"prestamos",
    component: ListadoPrestamoComponent,
    canActivate:[GuardGuard]
  },{
    path:"prestamos/crear",
    component: FormPrestamoComponent,
    canActivate:[GuardGuard]
  },{
    path:"",
    redirectTo: '/inicio',
    pathMatch:"full"
  },{
    path:"**",
    redirectTo: '/inicio',
  }
  ];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModuleModularizado { }
