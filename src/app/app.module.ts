import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppComponent } from './app.component';
/*
import { ListadoComponent } from './libros/listado/listado.component';
import { DetallesComponent } from './libros/detalles/detalles.component';
*/
import { InicioComponent } from './inicio/inicio.component';
import {AngularFireModule} from '@angular/fire/compat';
import {AngularFirestore} from '@angular/fire/compat/firestore';
import {environment} from "../environments/environment";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
/*
import { ListadoClientesComponent } from './clientes/listado-clientes/listado-clientes.component';
import { FormClientesComponent } from './clientes/form-clientes/form-clientes.component';
import { DetallesClientesComponent } from './clientes/detalles-clientes/detalles-clientes.component';
*/
import { FormPrestamoComponent } from './prestamo/form-prestamo/form-prestamo.component';
import { ListadoPrestamoComponent } from './prestamo/listado-prestamo/listado-prestamo.component';
//import { FormlibroComponent } from './libros/formlibro/formlibro.component';
import {AppRoutingModuleModularizado} from "./app-routing-modularizado.module";
import { MainLoginComponent } from './login/main-login/main-login.component';
import { RegisterComponent } from './login/register/register.component';
import {HeaderComponent} from "./header/header.component";




@NgModule({
  declarations: [
    AppComponent,
    InicioComponent,
    FormPrestamoComponent,
    ListadoPrestamoComponent,
    MainLoginComponent,
    RegisterComponent,
    HeaderComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModuleModularizado,
    FormsModule,
    ReactiveFormsModule,
    AngularFireModule.initializeApp(environment.firebaseConfig)
  ],
  providers: [AngularFirestore],
  bootstrap: [AppComponent]
})

export class AppModule { }
