export interface Libro {
  id:String;
  data:{
    titulo:String;
    isbn:String;
    autor:String;
    descripcion:String;
  }
}
