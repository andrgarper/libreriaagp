import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import {ClientesService} from "../../clientes/clientes.service";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {LibrosService} from "../libros.service";

@Component({
  selector: 'app-formlibro',
  templateUrl: './formlibro.component.html',
  styleUrls: ['./formlibro.component.css']
})
export class FormlibroComponent implements OnInit {

  libro: any = null;
  librosForm: any;
  rutaActual: any;
  esUpdate: boolean = false;
  patronIsbn ='[0-9]{10,13}';
  texto: string[] = ["Nuevo Libro", "Crear"];
  constructor(private router: Router, private ls:LibrosService) {
    //esto es para conseguir el libro que lo paso por post
    const navigation = this.router.getCurrentNavigation();
    this.rutaActual = navigation?.finalUrl?.root.children.primary.segments[1].path;
    console.log(this.rutaActual);
    if (this.rutaActual == "actualizar") {
      this.esUpdate = true;
      this.libro = navigation?.extras?.state;
      console.log(this.libro);
      //aqui cojo el librosform y le pongo los valores por defecto, bastante sencillo la verdad
      if (this.libro != null) {
        this.librosForm= new FormGroup({
          titulo: new FormControl(this.libro.data.titulo, Validators.required),
          isbn: new FormControl(this.libro.data.isbn, [Validators.required,Validators.pattern(this.patronIsbn)]),
          autor: new FormControl(this.libro.data.autor, Validators.required),
          descripcion: new FormControl(this.libro.data.descripcion, Validators.required)
        })
      }
    } else {
      this.librosForm= new FormGroup({
        titulo: new FormControl("", Validators.required),
        isbn: new FormControl("", [Validators.required,Validators.pattern(this.patronIsbn)]),
        autor: new FormControl("", Validators.required),
        descripcion: new FormControl("", Validators.required)
      });
    }
  }

  ngOnInit(): void {
    if (this.esUpdate) {
      if (this.libro == null) {
        //asi controlo que nadie se intente colar
        this.router.navigate(["libros"]);
      }
      this.texto = ["Actualizar Cliente", "Actualizar"];
    }
  }
  Actualizar() {
    if (this.librosForm.valid) {
      this.ls.updateLibro(this.libro.id,this.librosForm.value)
      this.router.navigate(["libros"]);
    }
  }

  Listado() {
    this.router.navigate(["libros"]);
  }

  esValido(campo: string) {
    const campoValidado = this.librosForm.get(campo);
    return (!campoValidado.valid && campoValidado.touched) ? 'is-invalid' : campoValidado.touched ? "is-valid" : "";
  }


  chequeado() {
    return (this.librosForm.valid) ? "" : "disabled";
  }

  Enviar() {
    if (this.esUpdate)
      this.Actualizar();
    else
      this.crear();
  }

  crear() {
    console.log(this.librosForm.valid)
    if (this.librosForm.valid) {
      this.ls.createLibro(this.librosForm.value)
      this.router.navigate(["libros"]);
    }
  }
}
