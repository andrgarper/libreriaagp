import { Component, OnInit } from '@angular/core';
import {NavigationExtras, Router} from "@angular/router";
import {LibrosService} from "../libros.service";

@Component({
  selector: 'app-detalles',
  templateUrl: './detalles.component.html',
  styleUrls: ['./detalles.component.css']
})
export class DetallesComponent implements OnInit {
  libro:any=null;
  navigationExtras:NavigationExtras={
    state:{
      value:null
    }}
  constructor(private router:Router,private ls:LibrosService) {
    //tengo q hacerlo en el constructor porque si lo hago en init me da null debido a que la navegacion muere al crearse
    //https://stackoverflow.com/questions/54891110/router-getcurrentnavigation-always-returns-null
    const navigation = this.router.getCurrentNavigation();
    this.libro= navigation?.extras?.state;
  }

  ngOnInit(): void {
    if (this.libro==null){
      //asi controlo que nadie se intente colar
      this.router.navigate(["libros"])
    }
  }
  Actualizar(libro:any){
    this.navigationExtras.state=libro;
    this.router.navigate(["libros/actualizar"],this.navigationExtras);
  }
  Borrar(libro:any){
    this.ls.deleteLibro(libro.id);
    console.log(libro)
    this.router.navigate(["libros"]);
  }
  Listado(){
    this.router.navigate(["libros"]);
  }

}
