import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {ListadoComponent} from "./listado/listado.component";
import {FormlibroComponent} from "./formlibro/formlibro.component";
import {DetallesComponent} from "./detalles/detalles.component";

const routes: Routes = [{
  path:"libros",
  component: ListadoComponent
},{
  path:"libros/nuevo",
  component: FormlibroComponent
},{
  path:"libros/detalles",
  component: DetallesComponent
},{
  path:"libros/actualizar",
  component: FormlibroComponent
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LibrosRoutingModule { }
