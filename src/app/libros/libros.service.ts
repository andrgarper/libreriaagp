import { Injectable } from '@angular/core';
import {Observable} from "rxjs";
import {Libro} from "./interfaces/libro";
import {AngularFirestore} from "@angular/fire/compat/firestore";

@Injectable({
  providedIn: 'root'
})
export class LibrosService {
  constructor(private firestore: AngularFirestore) { }
  public createLibro(libro:Libro) {
    return this.firestore.collection('libros').add(libro);
  }

  public getLibros(){
    return this.firestore.collection('libros').snapshotChanges();
  }

  public updateLibro(Id: string, libro: Libro) {
    return this.firestore.collection('libros').doc(Id).set(libro);
  }

  public deleteLibro(Id: string){
    return this.firestore.collection('libros').doc(Id).delete();
  }

  public  getLibro(Id: string){
    return this.firestore.collection('libros').doc(Id).snapshotChanges();
  }
}
