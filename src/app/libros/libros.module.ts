import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LibrosRoutingModule } from './libros-routing.module';
import {FormlibroComponent} from "./formlibro/formlibro.component";
import {ListadoComponent} from "./listado/listado.component";
import {DetallesComponent} from "./detalles/detalles.component";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";



@NgModule({
  declarations: [
    ListadoComponent,
    FormlibroComponent,
    DetallesComponent
  ],
  imports: [
    CommonModule,
    LibrosRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class LibrosModule { }
