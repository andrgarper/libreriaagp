import { Component, OnInit } from '@angular/core';
import {NavigationExtras, Router} from "@angular/router";
import {LibrosService} from "../libros.service";
import {Libro} from "../interfaces/libro";
import {PrestamosService} from "../../prestamo/prestamos.service";
import firebase from "firebase/compat/app";
import Timestamp = firebase.firestore.Timestamp;
import {AngularFireAuth} from "@angular/fire/compat/auth";

@Component({
  selector: 'app-listado',
  templateUrl: './listado.component.html',
  styleUrls: ['./listado.component.css']
})
export class ListadoComponent implements OnInit {

  libros: any = [];
  prestamos: any = [];
  navigationExtras: NavigationExtras = {
    state: {}
  }

  constructor(private router: Router, private ls: LibrosService, private ps: PrestamosService,private afauth:AngularFireAuth) {
  }

  ngOnInit(): void {

    /*this.afauth.currentUser.then(u=>{
      if (u?.email==undefined){
        this.router.navigate(["login"]);
      }})

     */

    this.ls.getLibros().subscribe((librosSnapshot) => {
      this.libros = [];
      librosSnapshot.forEach((libroData: any) => {
        this.libros.push({
          data: libroData.payload.doc.data(),
          id: libroData.payload.doc.id
        });
      })
    });
    this.ps.getPrestamos().subscribe((prestamosSnapshot) => {
      this.prestamos = [];
      prestamosSnapshot.forEach((prestamoData: any) => {
        this.prestamos.push({
          data: prestamoData.payload.doc.data(),
          id: prestamoData.payload.doc.id
        });
      })
      this.RellenaLibrosPrestamos()
    })

  }

  Crear() {
    this.router.navigate(["libros/nuevo"]);
  }

  Leer(libro: any) {
    this.navigationExtras.state = libro;
    this.router.navigate(["libros/detalles"], this.navigationExtras)
  }

  Actualizar(libro: any) {
    this.navigationExtras.state = libro;
    this.router.navigate(["libros/actualizar"], this.navigationExtras);
  }

  RellenaLibrosPrestamos() {
    for (let i = 0; i < this.libros.length; i++) {
      for (let j = 0; j < this.prestamos.length; j++) {
        if (this.libros[i].id == this.prestamos[j].data.lib_id && this.prestamos[j].data.fec_dev == null) {
          this.libros[i].prestamo = this.prestamos[j];
        }
      }
    }
  }
  EstaPrestado(libro:any){
    return (libro.prestamo!=null) ? "disabled":""

  }
  Devolver(prestamo:any){
    prestamo.data.fec_dev = Timestamp.now();
    this.ps.updatePrestamo(prestamo.id,prestamo.data);
    this.router.navigate(["prestamos"]);
  }
  CrearPrestamo(libro:any){
    this.navigationExtras.state = libro;
    this.router.navigate(["prestamos/crear"], this.navigationExtras);
  }
}
