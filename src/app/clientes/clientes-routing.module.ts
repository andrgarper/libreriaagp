import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {ListadoClientesComponent} from "./listado-clientes/listado-clientes.component";
import {FormClientesComponent} from "./form-clientes/form-clientes.component";
import {DetallesClientesComponent} from "./detalles-clientes/detalles-clientes.component";

const routes: Routes = [
  {
    path:"clientes",
    component: ListadoClientesComponent
  },{
    path:"clientes/nuevo",
    component: FormClientesComponent
  },{
    path:"clientes/detalles",
    component: DetallesClientesComponent
  },{
    path:"clientes/actualizar",
    component: FormClientesComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ClientesRoutingModule {

}
