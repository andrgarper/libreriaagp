import {Component, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {ClientesService} from "../clientes.service";

@Component({
  selector: 'app-form-clientes',
  templateUrl: './form-clientes.component.html',
  styleUrls: ['./form-clientes.component.css']
})
export class FormClientesComponent implements OnInit {

  cliente: any = null;
  clientesForm: any;
  patronIsbn = '';
  titulo = "";
  rutaActual: any;
  esUpdate: boolean = false;
  texto: string[] = ["Nuevo Cliente", "Crear"];


  constructor(private router: Router, private cs: ClientesService) {
    //esto es para conseguir el libro que lo paso por post
    const navigation = this.router.getCurrentNavigation();
    this.rutaActual = navigation?.finalUrl?.root.children.primary.segments[1].path;
    console.log(this.rutaActual);
    if (this.rutaActual == "actualizar") {
      this.esUpdate = true;
      this.cliente = navigation?.extras?.state;
      console.log(this.cliente);
      //aqui cojo el librosform y le pongo los valores por defecto, bastante sencillo la verdad
      if (this.cliente != null) {
        this.clientesForm = new FormGroup({
          nick: new FormControl(this.cliente.data.nick, Validators.required),
          email: new FormControl(this.cliente.data.email, [Validators.required, Validators.pattern(this.patronIsbn)]),
          contraseña: new FormControl(this.cliente.data.contraseña, Validators.required),
          rol: new FormControl(this.cliente.data.rol, Validators.required)
        });
      }
    } else {
      this.clientesForm = new FormGroup({
        nick: new FormControl("", Validators.required),
        email: new FormControl("", [Validators.required, Validators.pattern(this.patronIsbn)]),
        contraseña: new FormControl("", Validators.required),
        rol: new FormControl("", Validators.required)
      });
    }
  }

  ngOnInit(): void {
    if (this.esUpdate) {
      if (this.cliente == null) {
        //asi controlo que nadie se intente colar
        this.router.navigate(["clientes"]);
      }
      this.texto = ["Actualizar Cliente", "Actualizar"];
    }
  }

  Actualizar() {
    if (this.clientesForm.valid) {
      this.cs.updateCliente(this.cliente.id, this.clientesForm.value)
      this.router.navigate(["clientes"]);
    }
  }

  Listado() {
    this.router.navigate(["clientes"]);
  }

  esValido(campo: string) {
    const campoValidado = this.clientesForm.get(campo);
    return (!campoValidado.valid && campoValidado.touched) ? 'is-invalid' : campoValidado.touched ? "is-valid" : "";
  }


  chequeado() {
    return (this.clientesForm.valid) ? "" : "disabled";
  }

  Enviar() {
    if (this.esUpdate)
      this.Actualizar();
    else
      this.crear();
  }

  crear() {
    console.log(this.clientesForm.valid)
    if (this.clientesForm.valid) {
      this.cs.createCliente(this.clientesForm.value)
      this.router.navigate(["clientes"]);
    }
  }

}
