import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ListadoClientesComponent} from "./listado-clientes/listado-clientes.component"
import {FormClientesComponent} from "./form-clientes/form-clientes.component";
import {DetallesClientesComponent} from "./detalles-clientes/detalles-clientes.component";
import {ClientesRoutingModule} from "./clientes-routing.module";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";




@NgModule({
  declarations: [
    ListadoClientesComponent,
    FormClientesComponent,
    DetallesClientesComponent
  ],
  imports: [
    CommonModule,
    ClientesRoutingModule,
    FormsModule,
    ReactiveFormsModule

  ]
})
export class ClientesModule { }
