import { Component, OnInit } from '@angular/core';
import {NavigationExtras, Router} from "@angular/router";
import {ClientesService} from "../clientes.service";
import {AngularFireAuth} from "@angular/fire/compat/auth";

@Component({
  selector: 'app-listado-clientes',
  templateUrl: './listado-clientes.component.html',
  styleUrls: ['./listado-clientes.component.css']
})
export class ListadoClientesComponent implements OnInit {

  clientes:any=[];
  navigationExtras:NavigationExtras={
    state:{}
  }
  constructor(private router:Router,private cs:ClientesService,private afauth:AngularFireAuth) { }

  ngOnInit(): void {

   /* this.afauth.currentUser.then(u=>{
      if (u?.email==undefined){
        this.router.navigate(["login"]);
      }})
    */
    this.cs.getClientes().subscribe((clientesSnapshot) => {
      this.clientes=[];
      clientesSnapshot.forEach((clienteData: any) => {
        this.clientes.push({
          data: clienteData.payload.doc.data(),
          id: clienteData.payload.doc.id
        });
      })
    })
  }

  Crear(){
    this.router.navigate(["clientes/nuevo"]);
  }
  Leer(clientes:any){
    this.navigationExtras.state=clientes;
    this.router.navigate(["clientes/detalles"],this.navigationExtras)
  }
  Actualizar(clientes:any){
    this.navigationExtras.state=clientes;
    this.router.navigate(["clientes/actualizar"],this.navigationExtras);
  }
}
