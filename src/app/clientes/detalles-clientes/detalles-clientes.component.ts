import { Component, OnInit } from '@angular/core';
import {NavigationExtras, Router} from "@angular/router";
import {LibrosService} from "../../libros/libros.service";
import {ClientesService} from "../clientes.service";

@Component({
  selector: 'app-detalles-clientes',
  templateUrl: './detalles-clientes.component.html',
  styleUrls: ['./detalles-clientes.component.css']
})
export class DetallesClientesComponent implements OnInit {

  cliente:any=null;
  navigationExtras:NavigationExtras={
    state:{
      value:null
    }}
  constructor(private router:Router,private cs:ClientesService) {
    //tengo q hacerlo en el constructor porque si lo hago en init me da null debido a que la navegacion muere al crearse
    //https://stackoverflow.com/questions/54891110/router-getcurrentnavigation-always-returns-null
    const navigation = this.router.getCurrentNavigation();
    this.cliente= navigation?.extras?.state;
  }

  ngOnInit(): void {
    if (this.cliente==null){
      //asi controlo que nadie se intente colar
      this.router.navigate(["clientes"])
    }
  }
  Actualizar(cliente:any){
    this.navigationExtras.state=cliente;
    this.router.navigate(["clientes/actualizar"],this.navigationExtras);
  }
  Borrar(cliente:any){
    this.cs.deleteCliente(cliente.id);
    console.log(cliente)
    this.router.navigate(["clientes"]);
  }
  Listado(){
    this.router.navigate(["clientes"]);
  }


}
