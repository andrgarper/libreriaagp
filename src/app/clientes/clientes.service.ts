import { Injectable } from '@angular/core';
import {AngularFirestore} from "@angular/fire/compat/firestore";
import {Libro} from "../libros/interfaces/libro";

@Injectable({
  providedIn: 'root'
})
export class ClientesService {

  constructor(private firestore: AngularFirestore) { }
  public createCliente(cliente:any) {
    return this.firestore.collection('clientes').add(cliente);
  }

  public getClientes(){
    return this.firestore.collection('clientes').snapshotChanges();
  }

  public updateCliente(Id: string, cliente: any) {
    return this.firestore.collection('clientes').doc(Id).set(cliente);
  }

  public deleteCliente(Id: string){
    return this.firestore.collection('clientes').doc(Id).delete();
  }

  public  getCliente(Id: string){
    return this.firestore.collection('clientes').doc(Id).snapshotChanges();
  }

}
