import { Component, OnInit } from '@angular/core';
import {AngularFireAuth} from "@angular/fire/compat/auth";
import {Router} from "@angular/router";
import {LoginService} from "../login/login.service";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  userInfo = this.ls.getUserInfo();

  constructor(private router: Router, private afauth: AngularFireAuth,private ls: LoginService) {
  }

  ngOnInit(): void {
  }

  CerrarSesion() {
    this.afauth.signOut().then(u => {
      this.router.navigate(["login"]);
    })
  }
}




