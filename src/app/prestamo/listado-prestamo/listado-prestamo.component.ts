import { Component, OnInit } from '@angular/core';
import {NavigationExtras, Router} from "@angular/router";
//import {ClientesService} from "../../clientes/clientes.service";
import {PrestamosService} from "../prestamos.service";
//import {LibrosService} from "../../libros/libros.service";
import firebase from "firebase/compat/app";
import Timestamp = firebase.firestore.Timestamp;
import {AngularFireAuth} from "@angular/fire/compat/auth";


@Component({
  selector: 'app-listado-prestamo',
  templateUrl: './listado-prestamo.component.html',
  styleUrls: ['./listado-prestamo.component.css']
})
export class ListadoPrestamoComponent implements OnInit {


  prestamos: any = [];
  navigationExtras: NavigationExtras = {
    state: {}
  }

  constructor(private router: Router, private ps: PrestamosService,private afauth:AngularFireAuth) {
  }

  ngOnInit(): void {
    this.afauth.currentUser.then(u=>{
      if (u?.email==undefined){
        this.router.navigate(["login"]);
      }})

    this.ps.getPrestamos().subscribe((prestamosSnapshot) => {
      this.prestamos = [];
      prestamosSnapshot.forEach((prestamoData: any) => {
        this.prestamos.push({
          data: prestamoData.payload.doc.data(),
          id: prestamoData.payload.doc.id
        });
      })
    })

/* al no necesitar coger los libros ni los cliente para sacar el nombre no necesito chupar recursos
    this.cs.getClientes().subscribe((clientesSnapshot) => {
      this.clientes = [];
      clientesSnapshot.forEach((clienteData: any) => {
        this.clientes.push({
          data: clienteData.payload.doc.data(),
          id: clienteData.payload.doc.id
        });
      })
    })
    this.ls.getLibros().subscribe((librosSnapshot) => {
      this.libros = [];
      librosSnapshot.forEach((libroData: any) => {
        this.libros.push({
          data: libroData.payload.doc.data(),
          id: libroData.payload.doc.id
        });
      })
    })
*/
  }

/*
este metodo funcionaba para buscar los nick y titulo de cada libro
  RellenarPrestamos() {
    for (let i = 0; i < this.prestamos.length; i++) {

      for (let j = 0; j < this.libros.length; j++) {

        if (this.libros[j].id == this.prestamos[i].data.lib_id) {
          this.tituloLibro = this.libros[j].data.titulo;
        }
      }

      for (let z = 0; z < this.clientes.length; z++) {
        if (this.clientes[z].id == this.prestamos[i].data.cli_id) {
          this.nickCliente = this.clientes[z].data.nick;
        }
      }
      this.prestamos[i].data.lib_tit = this.tituloLibro;
      this.prestamos[i].data.cli_nick = this.nickCliente;
    }
  }
  */
  Devolver(prestamo:any){
    prestamo.data.fec_dev = Timestamp.now();
    this.ps.updatePrestamo(prestamo.id,prestamo.data);
    this.router.navigate(["prestamos"]);
  }
}

