import {Component, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {PrestamosService} from "../prestamos.service";
import {ClientesService} from "../../clientes/clientes.service";
import firebase from "firebase/compat/app";
import TIMESTAMP = firebase.firestore.Timestamp;


@Component({
  selector: 'app-form-prestamo',
  templateUrl: './form-prestamo.component.html',
  styleUrls: ['./form-prestamo.component.css']
})
export class FormPrestamoComponent implements OnInit {

  libro: any = null;
  prestamosForm: any;
  clientes: any = [];

  constructor(private router: Router, private ps: PrestamosService, private cs: ClientesService) {
    const navigation = this.router.getCurrentNavigation();
    this.libro = navigation?.extras?.state;
    this.prestamosForm = new FormGroup({
      cliente: new FormControl(null, Validators.required)
    })
  }

  ngOnInit(): void {
    if (this.libro == null) {
      //asi controlo que nadie se intente colar
      this.router.navigate(["libros"])
    }
    this.cs.getClientes().subscribe((clientesSnapshot) => {
      this.clientes = [];
      clientesSnapshot.forEach((clienteData: any) => {
        this.clientes.push({
          data: clienteData.payload.doc.data(),
          id: clienteData.payload.doc.id
        });
      })
      console.log(this.clientes)
    })
  }

  Crear() {
    let prestamo: any = {}
    let formdatos: any = null;
    if (this.prestamosForm.valid) {
      formdatos = this.prestamosForm.value.cliente;
      prestamo.cli_id = formdatos.id;
      prestamo.cli_nick = formdatos.data.nick;
      prestamo.lib_id = this.libro.id;
      prestamo.lib_tit = this.libro.data.titulo;
      prestamo.fec_prestamo = TIMESTAMP.now()
      this.ps.createPrestamo(prestamo);
      this.router.navigate(["prestamos"])
    }
  }

  Listado() {
    this.router.navigate(["libros"]);
  }
  chequeado() {
    return (this.prestamosForm.valid) ? "" : "disabled";
  }
}
