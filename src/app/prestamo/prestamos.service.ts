import { Injectable } from '@angular/core';
import {AngularFirestore} from "@angular/fire/compat/firestore";

@Injectable({
  providedIn: 'root'
})
export class PrestamosService {

  constructor(private firestore: AngularFirestore) {
  }

  public createPrestamo(prestamo: any) {
    return this.firestore.collection('prestamo').add(prestamo);
  }

  public getPrestamos() {
    return this.firestore.collection('prestamo').snapshotChanges();
  }

  public updatePrestamo(Id: string, prestamo: any) {
    return this.firestore.collection('prestamo').doc(Id).set(prestamo);
  }

  public deletePrestamo(Id: string) {
    return this.firestore.collection('prestamo').doc(Id).delete();
  }

  public getPrestamo(Id: string) {
    return this.firestore.collection('prestamo').doc(Id).snapshotChanges();
  }
}

