import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {ListadoComponent} from "./libros/listado/listado.component";
import {InicioComponent} from "./inicio/inicio.component";
import {DetallesComponent} from "./libros/detalles/detalles.component";
import {ListadoClientesComponent} from "./clientes/listado-clientes/listado-clientes.component";
import {FormClientesComponent} from "./clientes/form-clientes/form-clientes.component";
import {DetallesClientesComponent} from "./clientes/detalles-clientes/detalles-clientes.component";
import {ListadoPrestamoComponent} from "./prestamo/listado-prestamo/listado-prestamo.component";
import {FormPrestamoComponent} from "./prestamo/form-prestamo/form-prestamo.component";
import {FormlibroComponent} from "./libros/formlibro/formlibro.component";


const routes: Routes = [
  {
    path:"inicio",
    component: InicioComponent
  },{
    path:"libros",
    component: ListadoComponent
  },{
    path:"libros/nuevo",
    component: FormlibroComponent
  },{
    path:"libros/detalles",
    component: DetallesComponent
  },{
    path:"libros/actualizar",
    component: FormlibroComponent
  },{
    path:"clientes",
    component: ListadoClientesComponent
  },{
    path:"clientes/nuevo",
    component: FormClientesComponent
  },{
    path:"clientes/detalles",
    component: DetallesClientesComponent
  },{
    path:"clientes/actualizar",
    component: FormClientesComponent
  },{
    path:"prestamos",
    component: ListadoPrestamoComponent
  },{
    path:"prestamos/crear",
    component: FormPrestamoComponent
  },{
    path:"",
    redirectTo: '/inicio',
    pathMatch:"full"
  },{
    path:"**",
    redirectTo: '/inicio',
  }
  ];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
