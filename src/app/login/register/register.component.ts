import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import {LoginService} from "../login.service";
import {AngularFireAuth} from "@angular/fire/compat/auth";
import {FormControl, FormGroup, Validators} from "@angular/forms";

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  user: any = null;
  userForm: any;


  patronEmail = '^[\\w-\\.]+@([\\w-]+\\.)+[\\w-]{2,4}$';
  patronContraseña = '^[a-zA-Z0-9]{6,}$';

  constructor(private router: Router, private ls: LoginService,private afauth:AngularFireAuth) {
    //esto es para conseguir el libro que lo paso por post
    this.userForm = new FormGroup({
      email: new FormControl("", [Validators.required, Validators.pattern(this.patronEmail)]),
      contraseña: new FormControl("", [Validators.required, Validators.pattern(this.patronContraseña)]),
      repiteContraseña: new FormControl("", [Validators.required, Validators.pattern(this.patronContraseña)])
    });
  }

  ngOnInit(): void {
    this.afauth.signOut()
    console.log("he cerrado sesion si la hubiera")
  }

  Login() {
    this.router.navigate(["login"]);
  }

  esValido(campo: string) {
    const campoValidado = this.userForm.get(campo);
    return (!campoValidado.valid && campoValidado.touched) ? 'is-invalid' : campoValidado.touched ? "is-valid" : "";
  }


  chequeado() {
    return (this.userForm.valid&&this.sonIguales()) ? "" : "disabled";
  }


  Crear() {
    if (this.userForm.valid) {
      this.ls.createAccount(this.userForm.value.email, this.userForm.value.contraseña).then(u=>{
        u?.user?.sendEmailVerification()
        alert("mira el correo y verificalo")
      })
      this.router.navigate(["login"]);
    }
  }
  sonIguales(){
    return this.userForm.value.contraseña==this.userForm.value.repiteContraseña
  }

}
