import { Injectable } from '@angular/core';
import {AngularFireAuth} from "@angular/fire/compat/auth";
import firebase from 'firebase/compat/app';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private afauth:AngularFireAuth) { }
  async login(email:string,contraseña:string){
    try {
      return await this.afauth.signInWithEmailAndPassword(email,contraseña);
    }catch (e) {
      console.log(e)
      return null;
    }
  }

  async createAccount(email:string,contraseña:string){
    try {
      return await this.afauth.createUserWithEmailAndPassword(email,contraseña);
    }catch (e) {
      console.log(e)
      return null;
    }
  }

  getUserInfo() {
    return this.afauth.authState;
  }

  async loginWithGoogle() {
    try {
      return await this.afauth.signInWithPopup(new firebase.auth.GoogleAuthProvider())
    } catch (err) {
      console.log("error en login con Google: ", err);
      return null;
    }
  }

  logout() {
    this.afauth.signOut();
  }

  get isLoggedIn(){
    var loged = false;
    try {
      this.getUserInfo().subscribe(result =>{
        // this.userVerify = (result !== null && result.emailVerified !== false) ? true:false;
        loged = (result !== null);
      });
    } catch (err) {
      console.log("Error: " + err);
      return false;
    }
    return loged;
  }

}
