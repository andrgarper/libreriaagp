import {Component, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {LoginService} from "../login.service";
import {AngularFireAuth} from "@angular/fire/compat/auth";

@Component({
  selector: 'app-main-login',
  templateUrl: './main-login.component.html',
  styleUrls: ['./main-login.component.css']
})
export class MainLoginComponent implements OnInit {

  user: any = null;
  userForm: any;


  patronEmail = '^[\\w-\\.]+@([\\w-]+\\.)+[\\w-]{2,4}$';
  patronContraseña = '^[a-zA-Z0-9]{6,}$';

  constructor(private router: Router, private ls: LoginService, private afauth: AngularFireAuth) {
    //esto es para conseguir el libro que lo paso por post
    this.userForm = new FormGroup({
      email: new FormControl("", [Validators.required, Validators.pattern(this.patronEmail)]),
      contraseña: new FormControl("", [Validators.required, Validators.pattern(this.patronContraseña)]),
    });
  }

  ngOnInit(): void {
    this.afauth.signOut()
    console.log("he cerrado sesion si la hubiera")

  }

  Register() {
    this.router.navigate(["register"]);
  }

  esValido(campo: string) {
    const campoValidado = this.userForm.get(campo);
    return (!campoValidado.valid && campoValidado.touched) ? 'is-invalid' : campoValidado.touched ? "is-valid" : "";
  }


  chequeado() {
    return (this.userForm.valid) ? "" : "disabled";
  }


  iniciar() {

    if (this.userForm.valid) {
      this.ls.login(this.userForm.value.email, this.userForm.value.contraseña).then(u=>{
        if (u?.user?.emailVerified == true) {
          this.router.navigate(["inicio"]);
        } else if (u?.user?.emailVerified==false) {
          alert("No tienes el correo verificado,Mira tu correo")
        }
        else {
          alert("los datos no coinciden con ningun usuario o has puesto mal la contraseña o crea una cuenta")
        }
      })


    }
  }

  loginGoogle(){
    this.ls.loginWithGoogle().then(u=>{
      this.router.navigate(["inicio"]);
    })
  }

  olvidada(){
    if (this.userForm.get("email").valid){
      this.afauth.sendPasswordResetEmail(this.userForm.value.email).then(u=>{
        alert("se ha enviado un correo de recuperacion de contraseña a: "+ this.userForm.value.email)
      });
    }
  }
  chequeoEmail(){
    return (this.userForm.get("email").valid) ? "" : "disabled";
  }

}
