// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyAyennNekYmG88_YEfPFtSesEn5KA2-wX4",
    authDomain: "libreria-91ec8.firebaseapp.com",
    projectId: "libreria-91ec8",
    storageBucket: "libreria-91ec8.appspot.com",
    messagingSenderId: "277351081682",
    appId: "1:277351081682:web:c57fefdc27a4b67b12ef0b"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
